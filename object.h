//
// Created by Mark Keast (SC18MK)
//

#ifndef TRACKER_OBJECT_H
#define TRACKER_OBJECT_H


#include <string>
#include <pcl/tracking/kld_adaptive_particle_filter_omp.h>

using namespace pcl::tracking;
typedef pcl::PointXYZRGBA point;


class Object {
    public:
        std::string name;
        KLDAdaptiveParticleFilterOMPTracker<point, ParticleXYZRPY>::Ptr tracker;
        bool inScene = false;
        int hue, lowSat, highSat, lowVal, highVal;
        float currentSpeed;
        float averageSpeed;
        Object(int amountOfObjects);
        void recordState();
        std::vector<std::vector<float>> locations;
        std::vector<std::vector<double>> actualLocations;
        std::vector<time_t> times;
        std::vector<float> speeds;

    private:
        void calculateCurrentSpeed();
        void calculateAverageSpeed();
};


#endif //TRACKER_OBJECT_H

//
// Created by Mark Keast (SC18MK)
//

#include "box.h"
#include <pcl/common/transforms.h>
#include <iostream>

Box::Box(int amountOfObjects) : object(Object(amountOfObjects)), cornerCloud(new pcl::PointCloud<pcl::PointXYZRGBA>()) {}


// Calculates where a the required box corners are for use in "checkInBox" calculations
void Box::updateCornerCoordinates() {
    ParticleXYZRPY result = object.tracker->getResult();
    Eigen::Affine3f transformation = object.tracker->toEigenMatrix(result);

    // Uses known box dimensions and object position to calulate corner locations
    cornerCloud->points.clear();
    for (int i = 0; i < 4; i++) {
        pcl::PointXYZRGBA point;
        if (i == 1) {
            point.x = depth/2;
        } else {
            point.x = -depth/2;
        }
        
        if (i == 3) {
            point.y = height/2 + verticalOffset;
        } else {
            point.y = -height/2 + verticalOffset;
        }

        if (i == 2) {
            point.z = width/2;
        } else {
            point.z = -width/2;
        }
        
        cornerCloud->points.push_back(point);
    }

    pcl::transformPointCloud<point> (*cornerCloud, *cornerCloud, transformation);
}


// Checks whether the passed object is in the box
bool Box::checkInBox(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr object) {
    Eigen::Vector3f p1 = Eigen::Vector3f(cornerCloud->points[0].x, cornerCloud->points[0].y, cornerCloud->points[0].z);
    Eigen::Vector3f p2 = Eigen::Vector3f(cornerCloud->points[1].x, cornerCloud->points[1].y, cornerCloud->points[1].z);
    Eigen::Vector3f p3 = Eigen::Vector3f(cornerCloud->points[2].x, cornerCloud->points[2].y, cornerCloud->points[2].z);
    Eigen::Vector3f p4 = Eigen::Vector3f(cornerCloud->points[3].x, cornerCloud->points[3].y, cornerCloud->points[3].z);
    Eigen::Vector3f u = p1 - p2;
    Eigen::Vector3f v = p1 - p3;
    Eigen::Vector3f w = p1 - p4;

    // Checks whether a point is within the points p1, p2, p3 and p4 using the line equations between them u, v and w
    for (const auto& point: object->points) {
        bool uSatisfied = false;
        bool vSatisfied = false;
        bool wSatisfied = false;

        Eigen::Vector3f pointToCheck = Eigen::Vector3f(point.x, point.y, point.z);

        // Checks whether a point is between p1 and p2 using line equation u
        if ((u.dot(pointToCheck) > u.dot(p1) && u.dot(pointToCheck) < u.dot(p2)) || (u.dot(pointToCheck) < u.dot(p1) && u.dot(pointToCheck) > u.dot(p2))) {
            uSatisfied = true;
        } 

        // Checks whether a point is between p1 and p3 using line equation v
        if ((v.dot(pointToCheck) > v.dot(p1) && v.dot(pointToCheck) < v.dot(p3)) || (v.dot(pointToCheck) < v.dot(p1) && v.dot(pointToCheck) > v.dot(p3))) {
            vSatisfied = true;
        } 

        // Checks whether a point is between p1 and p4 using line equation w
        if ((w.dot(pointToCheck) > w.dot(p1) && w.dot(pointToCheck) < w.dot(p4)) || (w.dot(pointToCheck) < w.dot(p1) && w.dot(pointToCheck) > w.dot(p4))) {
            wSatisfied = true;
        } 

        // If all satisfied then point is in the box
        if (uSatisfied && vSatisfied && wSatisfied) {
            return true;
        } 
    }

    return false;
}
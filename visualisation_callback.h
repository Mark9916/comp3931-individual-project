//
// Created by Mark Keast (SC18MK)
//

#ifndef VISUALISATION_CALLBACK_H
#define VISUALISATION_CALLBACK_H


#include "globals.h"


void visualisationCallback(pcl::visualization::PCLVisualizer &visualiser);


#endif //VISUALISATION_CALLBACK_H
//
// Created by Mark Keast (SC18MK)
//

#include "globals.h"
#include "tracking.h"
#include "visualisation_callback.h"
#include "cloud_callback.h"
#include "test.h"
#include <thread>
#include <pcl/io/openni_grabber.h>
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include <boost/bind.hpp>

using namespace std::chrono_literals;

pcl::visualization::CloudViewer viewer("Synoptic Project - Mark Keast");
std::map<std::string, std::vector<double>> objectActualLocations;
bool useCorrection;
bool hideWhenLost;


// Saves the object locations from OptiTrack
void saveObjectActualLocation(const geometry_msgs::PoseStamped::ConstPtr& msg, string object) {
    std::vector<double> location{msg->pose.position.x, msg->pose.position.y, msg->pose.position.z, msg->pose.orientation.w, msg->pose.orientation.x, msg->pose.orientation.y, msg->pose.orientation.z};
    objectActualLocations[object] = location;
}

// Setups up the callbacks for the cloud and visualisation threads.
void setupInterface(vector<vector<string>> boxParameters, vector<vector<string>> objectParameters) {
    // Gets input from the camera
    pcl::Grabber* interface = new pcl::OpenNIGrabber();

    // Visualisation callback
    function<void (pcl::visualization::PCLVisualizer&)> visualisationCallbackFunction = visualisationCallback;
    viewer.runOnVisualizationThread(visualisationCallbackFunction, "visualisationCallbackFunction");

    // Test callback
    ros::NodeHandle node;
    ros::Subscriber testSubscriber = node.subscribe("/record_flag", 1000, test);

    // OptiTrack callback
    vector<vector<string>> allObjectParameters = boxParameters;
    allObjectParameters.insert(allObjectParameters.end(), objectParameters.begin(), objectParameters.end());
    vector<ros::Subscriber> objectSubscribers;
    for (auto &objectParameters: allObjectParameters) {
        ros::Subscriber optiTrackSubscriber = node.subscribe<geometry_msgs::PoseStamped>("/mocap/rigid_bodies/" + objectParameters[0] + "/pose", 1, boost::bind(saveObjectActualLocation, _1, objectParameters[0]));
        objectSubscribers.push_back(optiTrackSubscriber);
    }

    // Cloud callback
    function<void (const pcl::PointCloud<point>::ConstPtr&)> callbackFunction = cloudCallback;
    interface->registerCallback(callbackFunction);

    // Loop to keep application running
    interface->start();
    while (!viewer.wasStopped()) {
        ros::spinOnce();
        this_thread::sleep_for(chrono::milliseconds(50));
    }
    interface->stop ();
}


// Starts the tracker
void run(vector<vector<string>> boxParameters, vector<vector<float>> boxDimensions, vector<vector<string>> objectParameters) {
    setupTracking(boxParameters, boxDimensions, objectParameters);
    setupInterface(boxParameters, objectParameters);
}


// Place to set box and object parameters
int main(int argc, char **argv) {
    ros::init(argc, argv, "point_cloud_tracker");

    // Application Options
    useCorrection = true;
    hideWhenLost = true;

    // Boxs
    vector<vector<string>> boxParameters{{"Box 1", "/home/mark_keast/Documents/University Work/COMP3932 Synoptic Project/program/objects/box.ply", "20", "60", "245", "10", "105"},
                                         // {"Box 2", "/home/mark_keast/Documents/University Work/COMP3932 Synoptic Project/program/objects/box_2.ply", "20", "60", "245", "10", "105"}
                                         };

    // Box Width, Height, Depth, Vertical Offset
    vector<vector<float>> boxDimensions{{0.377, 0.253, 0.235, 0.027},
                                        //{0.185, 0.261, 0.292, 0.02}
                                        };

    // Objects
    vector<vector<string>> objectParameters{//{"Rubiks_Cube", "/home/mark_keast/Documents/University Work/COMP3932 Synoptic Project/program/objects/077_rubiks_cube/clouds/merged_cloud.ply", "28", "200", "255", "110", "255"},
                                            //{"Pringles", "/home/mark_keast/Documents/University Work/COMP3932 Synoptic Project/program/objects/001_chips_can/clouds/merged_cloud.ply", "3", "170", "255", "70", "230"},
                                            //{"Tennis Ball", "/home/mark_keast/Documents/University Work/COMP3932 Synoptic Project/program/objects/056_tennis_ball/clouds/merged_cloud.ply", "41", "100", "255", "50", "200"},
                                            {"Rubiks_Cube", "/home/mark_keast/Documents/University Work/COMP3932 Synoptic Project/program/objects/Rubik cube.ply", "28", "200", "255", "110", "255"},
                                            {"Jello", "/home/mark_keast/Documents/University Work/COMP3932 Synoptic Project/program/objects/Jello Box.ply", "41", "100", "255", "50", "200"},
                                            {"Pringles", "/home/mark_keast/Documents/University Work/COMP3932 Synoptic Project/program/objects/Pringles.ply", "3", "170", "255", "70", "230"}
                                            };


    // Validation to stop more than 6 objects
    if (boxParameters.size() + objectParameters.size() > 6) {
        cout << "The maximum amount of objects has been exceeded! (6 total)" << endl;
        exit(-1);
    }

    // Validation to make sure all boxs have dimensions
    if (boxParameters.size() != boxDimensions.size()) {
        cout << "The amount of boxes to box dimensions specified is not consistent!" << endl;
        exit(-1);
    }

    // Starts the application
    run(boxParameters, boxDimensions, objectParameters);

    return 0;
}

# COMP3931 Synoptic Project - Mark Keast

This is my 4th year individual project repository. This project implemented code to track multiple objects in 6D using point clouds and work outs whether those objects have been placed into boxs.

Prerequisites:
- A C compiler.
- CMake.
- Catkin.
- PCL (Point Cloud Library).
- ROS.
- OpenCV.
- CAD models in ply or pcd format of the object you want to track.
- CAD models in ply or pcd format of the boxs you want to track.
- A local network with ROS topics for "/record_flag and "/mocap/rigid_bodies/{object name}/pose" for each object.

Usage:
- In an empty directory create a directory called "src".
- In the directory containing "src" run "catkin_make".
- Navigate to the "src" folder and run "catkin_create_pkg synoptic_project".
- Navigate to "src/synoptic_project/src" and pull this git repository.
- Specify the object names, object filepaths, object colours, box names, box filepaths, box colours and box dimensions in main.cpp.
- Compile the program using "catkin_make".
- Run the executable "node" found in "devel/lib/synoptic_project/"

Notes:
- I found Windows to not work well with both PCL and the Asus Xtion Pro camera that I used. I would recommend using Linux for the running of this program.

//
// Created by Mark Keast (SC18MK)
//

#ifndef GLOBALS_H
#define GLOBALS_H


#include "box.h"
#include "object.h"
#include <pcl/visualization/cloud_viewer.h>


typedef pcl::PointXYZRGBA point;
extern pcl::visualization::CloudViewer viewer;
extern pcl::PointCloud<point>::Ptr globalCloud;
extern pcl::PointCloud<point>::Ptr globalFilteredCloud;
extern bool newGlobalCloud;
extern std::vector<Box> boxs;
extern std::vector<Object> objects;
extern std::map<std::string, std::vector<double>> objectActualLocations;
extern std::mutex mtx;
extern int count;
extern float averageFPS;
extern bool useCorrection;
extern bool hideWhenLost;


#endif //GLOBALS_H


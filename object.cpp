//
// Created by Mark Keast (SC18MK)
//

#include "object.h"
#include "globals.h"
#include <chrono>
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"

Object::Object(int amountOfObjects) : tracker(new KLDAdaptiveParticleFilterOMPTracker<point, ParticleXYZRPY>(8 / amountOfObjects)) {}


// Records the state of an object (location and time)
void Object::recordState() {
    // Stores all the required values
    ParticleXYZRPY result = tracker->getResult();
    Eigen::Affine3f transformation = tracker->toEigenMatrix(result);
    Eigen::Matrix3f rotation = transformation.rotation();
    std::vector<float> location{result.x, result.y, result.z, result.roll, result.pitch, result.yaw, rotation(0,0), rotation(0,1), rotation(0,2), rotation(1,0), rotation(1,1), rotation(1,2), rotation(2,0), rotation(2,1), rotation(2,2)};
    locations.push_back(location);
    actualLocations.push_back(objectActualLocations[name]);
    auto time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    times.push_back(time);
    calculateCurrentSpeed();
    calculateAverageSpeed();
}


// Calculates the current speed of an object
void Object::calculateCurrentSpeed() {
    if (locations.size() >= 2 && times.size() >= 2) {
        // Uses distance (delta between last 2 locations) divided by time (delta between last 2 locations)
        std::vector<float> l1 = locations[locations.size()-1];
        std::vector<float> l2 =  locations[locations.size()-2];
        float distanceDelta = pow(pow(l2[0]-l1[0], 2) + pow(l2[1]-l1[1], 2) + pow(l2[2]-l1[2], 2), 0.5);
        float timeDelta = float(times[times.size()-1] - times[times.size()-2])/1000;
        float speed = distanceDelta/timeDelta;
        speeds.push_back(speed);
        currentSpeed = speed;
    } 
}


// Calculates the average speed of an object
void Object::calculateAverageSpeed() {
    if (locations.size() >= 2 && times.size() >= 2) {
        // Uses sum of distances (delta between all locations) divided by sum of times (delta between all locations)
        float totalDistanceDelta = 0;
        for (int i = 1; i < locations.size(); i++) {
            std::vector<float> l1 = locations[i];
            std::vector<float> l2 =  locations[i-1];
            totalDistanceDelta += pow(pow(l2[0]-l1[0], 2) + pow(l2[1]-l1[1], 2) + pow(l2[2]-l1[2], 2), 0.5);
        }

        float totalTimeDelta = float(times[times.size()-1] - times[0])/1000;
        averageSpeed =  totalDistanceDelta/totalTimeDelta;
    } 
}
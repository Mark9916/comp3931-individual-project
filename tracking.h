//
// Created by Mark Keast (SC18MK)
//

#ifndef TRACKING_H
#define TRACKING_H


#include "globals.h"

using namespace std;


void setupTracking(vector<vector<string>> boxParameters, vector<vector<float>> boxDimensions, vector<vector<string>> objectParameters);


#endif //TRACKING_H

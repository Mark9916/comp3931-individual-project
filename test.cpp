//
// Created by Mark Keast (SC18MK)
//

#include "test.h"
#include <pcl/visualization/keyboard_event.h>

bool inTest = false;


// Starts and Records Test Results
void test(const std_msgs::String::ConstPtr& msg) {
    std::lock_guard<std::mutex> lock(mtx);

    if (!inTest) {
        count = 0;

        // Resets global variables ready to test
        for(auto &box: boxs) {
            box.object.locations.clear();
            box.object.actualLocations.clear();
            box.object.times.clear();
            box.object.speeds.clear();
        }

        for(auto &object: objects) {
            object.locations.clear();
            object.actualLocations.clear();
            object.times.clear();
            object.speeds.clear();
        }

        std::cout << "-------------------------------------------------------------------" << std::endl;
        std::cout << "Testing" << std::endl;
        std::cout << "-------------------------------------------------------------------" << std::endl;
        std::cout << std::endl;

        inTest = true;
    } else {
        std::vector<Object> localObjects;
        for(auto &box: boxs) {
            localObjects.push_back(box.object);
        }
        localObjects.insert(localObjects.end(), objects.begin(), objects.end());

        // Prints the test results
        std::cout << "-------------------------------------------------------------------" << std::endl;
        std::cout << "Test Results:" << std::endl;
        std::cout << "   Average FPS: " << averageFPS << std::endl;
        std::cout << "-------------------------------------------------------------------" << std::endl;

        for (auto &object: localObjects) {
            // Writes the results to CSV files
            std::ofstream csvFile("/home/mark_keast/Documents/University Work/COMP3932 Synoptic Project/catkin_program/catkin_ws/src/synoptic_project/src/Test Results/" + msg->data + "_" + object.name + ".csv");
            csvFile << "Time,X,Y,Z,Roll,Pitch,Yaw,r(0:0),r(0:1),r(0:2),r(1:0),r(1:1),r(1:2),r(2:0),r(2:1),r(2:2),p.x,p.y,p.z,o.w,o.x,o.y,o.z,Average FPS" << std::endl;
            int i = 0;
            for (auto &location: object.locations) {
                csvFile << object.times[i] << "," << location[0] << "," << location[1] << "," << location[2] << "," << location[3] << ","
                << location[4] << "," << location[5] << "," << location[6] << "," << location[7] << "," << location[8] << ","
                << location[9] << "," << location[10] << "," << location[11] << "," << location[12] << "," << location[13] << ","
                << location[14] << "," << object.actualLocations[i][0] << "," << object.actualLocations[i][1] << "," << object.actualLocations[i][2]<< ","
                << object.actualLocations[i][3]<< "," << object.actualLocations[i][4] << "," << object.actualLocations[i][5] << ","
                << object.actualLocations[i][6] << "," << averageFPS << std::endl;
                i++;
            }
            csvFile.close();

            std::cout << object.name << ":" << std::endl;
            
            std::cout << "  Object Locations (X, Y, Z, Roll, Pitch, Yaw):" << std::endl;
            for (auto &location: object.locations) {
                cout << "    " << location[0] << " " << location[1] << " " << location[2] << " " << location[3] << " " << location[4] << " " << location[5] << std::endl;
            }

            std::cout << std::endl;
            std::cout << "  Object Times:" << std::endl;
            for (auto &time: object.times) {
                cout << "    " << time << std::endl;
            }

            std::cout << std::endl;
            std::cout << "  Object Average Speed: " << object.averageSpeed << std::endl;

            std::cout << std::endl;
            std::cout << "  Object Speeds: " << std::endl;
            for (auto &speed: object.speeds) {
                cout << "    " << speed << std::endl;
            }

            std::cout << "-------------------------------------------------------------------" << std::endl;
        }
        std::cout << std::endl;

        inTest = false;
    }
}
//
// Created by Mark Keast (SC18MK)
//

#include "tracking.h"
#include "cloud_callback.h"
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/tracking/distance_coherence.h>
#include <pcl/tracking/hsv_color_coherence.h>
#include <pcl/tracking/approx_nearest_pair_point_cloud_coherence.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/search/pcl_search.h>

using namespace pcl::tracking;

vector<Box> boxs;
vector<Object> objects;


// Sets up the objects for tracking
void setupTracking(vector<vector<string>> boxParameters, vector<vector<float>> boxDimensions, vector<vector<string>> objectParameters) {
    // Combines box and object parameters to allow object initiation in one loop
    int amountOfBoxs = boxParameters.size();
    int amountOfObjects = objectParameters.size();
    vector<vector<string>> allObjectParameters = boxParameters;
    allObjectParameters.insert(allObjectParameters.end(), objectParameters.begin(), objectParameters.end());

    for(int i = 0; i < amountOfBoxs + amountOfObjects; i++) {
        Object object(amountOfBoxs + amountOfObjects);
        object.name = allObjectParameters[i][0];
        object.hue = stoi(allObjectParameters[i][2]);
        object.lowSat = stoi(allObjectParameters[i][3]);
        object.highSat = stoi(allObjectParameters[i][4]);
        object.lowVal = stoi(allObjectParameters[i][5]);
        object.highVal = stoi(allObjectParameters[i][6]);

        // Read object file (pcd or ply formats accepted)
        pcl::PointCloud<point>::Ptr targetCloud(new pcl::PointCloud<point>());
        if(allObjectParameters[i][1].substr(allObjectParameters[i][1].find_last_of(".") + 1) == "ply") {
            if(pcl::io::loadPLYFile(allObjectParameters[i][1], *targetCloud) == -1) { 
                cout << "Could not find object tracking file!" << endl;
                exit(-1);
            }
        } else if (allObjectParameters[i][1].substr(allObjectParameters[i][1].find_last_of(".") + 1) == "pcd") {
            if(pcl::io::loadPCDFile(allObjectParameters[i][1], *targetCloud) == -1) { 
                cout << "Could not find object tracking file!" << endl;
                exit(-1);
            }
        } else {
            cout << "One or more object files are of an incorrect format!" << endl;
            exit(-1);
        }
        
        vector<double> defaultStepCovariance = vector<double>(6, 0.015 * 0.015);
        defaultStepCovariance[3] *= 40.0;
        defaultStepCovariance[4] *= 40.0;
        defaultStepCovariance[5] *= 40.0;

        vector<double> initialNoiseCovariance = vector<double>(6, 0.00001);
        vector<double> defaultInitialMean = vector<double>(6, 0.0);

        ParticleXYZRPY binSize;
        binSize.x = 0.1f;
        binSize.y = 0.1f;
        binSize.z = 0.1f;
        binSize.roll = 0.1f;
        binSize.pitch = 0.1f;
        binSize.yaw = 0.1f;

        // Sets all parameters for the tracker
        object.tracker->setMaximumParticleNum(1000);
        object.tracker->setDelta(0.99);
        object.tracker->setEpsilon(0.2);
        object.tracker->setBinSize(binSize);
        object.tracker->setTrans(Eigen::Affine3f::Identity());
        object.tracker->setStepNoiseCovariance(defaultStepCovariance);
        object.tracker->setInitialNoiseCovariance(initialNoiseCovariance);
        object.tracker->setInitialNoiseMean(defaultInitialMean);
        object.tracker->setIterationNum(1);
        object.tracker->setParticleNum(600);
        object.tracker->setResampleLikelihoodThr(0.00);
        object.tracker->setUseNormal(false);

        // Sets up coherence object for tracking
        ApproxNearestPairPointCloudCoherence<point>::Ptr coherence
            (new ApproxNearestPairPointCloudCoherence<point>);

        // Adds distance to the likelihood function;
        DistanceCoherence<point>::Ptr distanceCoherence
            (new DistanceCoherence<point>);
        distanceCoherence->setWeight(0.75);
        coherence->addPointCoherence(distanceCoherence);

        // Adds colour to the likelihood function
        HSVColorCoherence<point>:: Ptr colourCoherence
            (new HSVColorCoherence<point>);
        colourCoherence->setWeight(0.1);
        coherence->addPointCoherence(colourCoherence);   

        // Sets the search method
        pcl::search::Octree<point>::Ptr search(new pcl::search::Octree<point>(0.01));
        coherence->setSearchMethod(search);
        coherence->setMaximumDistance(0.01);

        object.tracker->setCloudCoherence(coherence);

        // Prepare the model of tracker's target (Centers object then downsamples)
        Eigen::Vector4f centroid;
        Eigen::Affine3f translation = Eigen::Affine3f::Identity();
        pcl::compute3DCentroid<point>(*targetCloud, centroid);
        translation.translation().matrix() = Eigen::Vector3f(centroid[0], centroid[1], centroid[2]); 
        translation.rotate(Eigen::AngleAxisf(4.71239, Eigen::Vector3f::UnitX()));
        pcl::transformPointCloud<point>(*targetCloud, *targetCloud, translation.inverse());
        voxelGridDownSampling(targetCloud, targetCloud, 0.01);
        statisticalOutlierRemoval(targetCloud, targetCloud);

        // Set reference model and translation
        object.tracker->setReferenceCloud(targetCloud);
        object.tracker->setTrans(translation);

        // Stores the created object
        if (i > amountOfBoxs - 1) {
            objects.push_back(object);
        } else {
            Box box(amountOfBoxs + amountOfObjects);
            box.object = object;
            box.width = boxDimensions[i][0];
            box.height = boxDimensions[i][1];
            box.depth = boxDimensions[i][2];
            box.verticalOffset = boxDimensions[i][3];
            boxs.push_back(box);
        }
    }
}
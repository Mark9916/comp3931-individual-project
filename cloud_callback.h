//
// Created by Mark Keast (SC18MK)
//

#ifndef CLOUD_CALLBACK_H
#define CLOUD_CALLBACK_H


#include "globals.h"


void calculateFPS();
pcl::PointCloud<point>::Ptr openCVClustering(const pcl::PointCloud<point>::ConstPtr &cloud, int hue, int saturation, int value);
void calculateObjectPositions(const pcl::PointCloud<point>::ConstPtr &cloud);
void statisticalOutlierRemoval(const pcl::PointCloud<point>::ConstPtr &cloud, pcl::PointCloud<point>::Ptr &filteredCloud);
void planarSegmentation(const pcl::PointCloud<point>::ConstPtr &cloud, pcl::PointCloud<point>::Ptr &filteredCloud, double distanceThreshold);
void voxelGridDownSampling(const pcl::PointCloud<point>::ConstPtr &cloud, pcl::PointCloud<point>::Ptr &filteredCloud, float leafSize);
void passThroughFilter(const pcl::PointCloud<point>::ConstPtr &cloud, pcl::PointCloud<point>::Ptr &filteredCloud, float x, float y, float z);
void cloudCallback(const pcl::PointCloud<point>::ConstPtr &cloud);


#endif //CLOUD_CALLBACK_H
//
// Created by Mark Keast (SC18MK)
//

#ifndef TEST_H
#define TEST_H


#include "globals.h"
#include "std_msgs/String.h"


void test(const std_msgs::String::ConstPtr& msg);


#endif //TEST_H

//
// Created by Mark Keast (SC18MK)
//

#include "cloud_callback.h"
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/registration/sample_consensus_prerejective.h>
#include <pcl_conversions/pcl_conversions.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

pcl::PointCloud<point>::Ptr globalCloud(new pcl::PointCloud<point>());
pcl::PointCloud<point>::Ptr globalFilteredCloud(new pcl::PointCloud<point>());
const float globalLeafSize = 0.01;
bool newGlobalCloud = false;
std::mutex mtx;
int count = 0;
int64_t startTime;
float averageFPS = 0;
int frameCount;


// Calculates the average FPS of the cloud callback function
void calculateFPS() {
    // Uses total frames divided by total time elapsed
    frameCount++;
    auto currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    float timeDelta = float(currentTime - startTime)/1000;
    averageFPS = frameCount/timeDelta;
}


// Creates a cluster in the point cloud for passed colour hue, saturation and value
pcl::PointCloud<point>::Ptr openCVClustering(const pcl::PointCloud<point>::ConstPtr &cloud, int hue, int lowSat, int highSat, int lowVal, int highVal) {
    // Converts the point cloud to a usable image format
    sensor_msgs::Image sensorImage;
    cv_bridge::CvImagePtr image;
    cv::Mat imageHSV;
    pcl::toROSMsg(*cloud, sensorImage);
    image = cv_bridge::toCvCopy(sensorImage, sensor_msgs::image_encodings::BGR8);
    cv::cvtColor(image->image, imageHSV, CV_BGR2HSV);

    // Mask to pick out a HSV colour
    cv::Mat1b mask;
    if ((hue - 6) < 0) {
        cv::Mat1b mask1, mask2;
        cv::inRange(imageHSV, cv::Scalar(0, lowSat, lowVal), cv::Scalar(hue + 6, highSat, highVal), mask1);
        cv::inRange(imageHSV, cv::Scalar(174 + hue, lowSat, lowVal), cv::Scalar(180, highSat, highVal), mask2);
        mask = mask1 | mask2;
    } else if ((hue + 6) > 180) {
        cv::Mat1b mask1, mask2;
        cv::inRange(imageHSV, cv::Scalar(0, lowSat, lowVal), cv::Scalar(hue - 174, highSat, highVal), mask1);
        cv::inRange(imageHSV, cv::Scalar(hue - 6, lowSat, lowVal), cv::Scalar(180, highSat, highVal), mask2);
        mask = mask1 | mask2;
    } else {
        cv::inRange(imageHSV, cv::Scalar(hue - 6, lowSat, lowVal), cv::Scalar(hue + 6, highSat, highVal), mask);
    }

    // Mask viewer for testing
    // cv::namedWindow("Mask", cv::WINDOW_AUTOSIZE);
    // cv::imshow("Mask", mask);
    // cv::waitKey(0);

    // Finds points in point cloud that match mask
    pcl::PointCloud<point>::Ptr cluster(new pcl::PointCloud<point>());
    pcl::PointCloud<point>::Ptr filteredCloud(new pcl::PointCloud<point>());
    passThroughFilter(cloud, filteredCloud, 0.65, 1, 1.80);
    float cameraParameter = 527;
    for (auto& point: filteredCloud->points) {
        if (!isnan(point.x) && !isnan(point.y)) {
            // Conversion of coordinates from point cloud space to image space
            int xFrame = floor(point.x * cameraParameter/point.z) + 320;
            int yFrame = floor(point.y * cameraParameter/point.z) + 240; 
            if (mask.at<uchar>(yFrame, xFrame) == 255) {
                cluster->points.push_back(point);
            }
        }
    }

    // Adjusting the cloud so it is the same resolution as the object reference models
    voxelGridDownSampling(cluster, cluster, globalLeafSize);
    statisticalOutlierRemoval(cluster, cluster);

    return cluster;
}


// Works out where an object is and its pose in a scene (point cloud registration)
void calculateObjectPosition(const pcl::PointCloud<point>::ConstPtr &scene) {
    std::vector<Object> localObjects;
    for (auto &box: boxs) {
        localObjects.push_back(box.object);
    }
    localObjects.insert(localObjects.end(), objects.begin(), objects.end());

    int i = 0;
    for(auto &object: localObjects) {
        // Uses colour to come up with a proposed location (a cluster of points)
        pcl::PointCloud<point>::Ptr cluster = openCVClustering(scene, object.hue, object.lowSat, object.highSat, object.lowVal, object.highVal);
        pcl::PointCloud<point>::ConstPtr objectReference = object.tracker->getReferenceCloud();
        if (cluster->size() > 50) {
            pcl::PointCloud<point>::Ptr objectAlligned(new pcl::PointCloud<point>());
            pcl::PointCloud<pcl::Normal>::Ptr objectNormals(new pcl::PointCloud<pcl::Normal>());
            pcl::PointCloud<pcl::Normal>::Ptr sceneNormals(new pcl::PointCloud<pcl::Normal>());
            pcl::PointCloud<pcl::FPFHSignature33>::Ptr objectFeatures(new pcl::PointCloud<pcl::FPFHSignature33>());
            pcl::PointCloud<pcl::FPFHSignature33>::Ptr sceneFeatures(new pcl::PointCloud<pcl::FPFHSignature33>());
            pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr searchMethod(new pcl::search::KdTree<pcl::PointXYZRGBA>());;

            // Estimates normals for scene
            pcl::NormalEstimationOMP<point, pcl::Normal> normalEstimation;
            normalEstimation.setSearchMethod(searchMethod);
            normalEstimation.setRadiusSearch(0.01);
            normalEstimation.setInputCloud(objectReference);
            normalEstimation.compute(*objectNormals);
            normalEstimation.setInputCloud(cluster);
            normalEstimation.compute(*sceneNormals);
            
            // Estimates features
            pcl::FPFHEstimationOMP<point, pcl::Normal, pcl::FPFHSignature33> FPFHEstimation;
            FPFHEstimation.setRadiusSearch(0.025);
            FPFHEstimation.setInputCloud(objectReference);
            FPFHEstimation.setInputNormals(objectNormals);
            FPFHEstimation.compute(*objectFeatures);
            FPFHEstimation.setInputCloud(cluster);
            FPFHEstimation.setInputNormals(sceneNormals);
            FPFHEstimation.compute(*sceneFeatures);
            
            // Performs alignment
            pcl::SampleConsensusPrerejective<point, point, pcl::FPFHSignature33> align;
            align.setInputSource(objectReference);
            align.setSourceFeatures(objectFeatures);
            align.setInputTarget(cluster);
            align.setTargetFeatures(sceneFeatures);
            align.setMaximumIterations(50000); // Number of RANSAC iterations
            align.setNumberOfSamples(3); // Number of points to sample for generating/prerejecting a pose
            align.setCorrespondenceRandomness(5); // Number of nearest features to use
            align.setSimilarityThreshold(0.90f); // Polygonal edge length similarity threshold
            align.setMaxCorrespondenceDistance(2.5f * globalLeafSize); // Inlier threshold
            align.setInlierFraction(0.25f); // Percentage of inliers for accepting a hypothesis
            align.align(*objectAlligned);
            
            // If an allignement is found it passes the transformation to the tracker
            if (align.hasConverged()) {
                // Alignment viewer for testing
                // pcl::console::print_info("Inliers: %i/%i\n", align.getInliers().size(), objectReference->size());
                // pcl::visualization::PointCloudColorHandlerCustom<point> cloudColour(objectAlligned, 0, 255, 0);
                // pcl::visualization::PCLVisualizer view("Alignment");
                // view.addPointCloud(scene, "scene");
                // view.addPointCloud(objectAlligned, cloudColour, "objectAlligned");
                // view.spin();

                Eigen::Affine3f trackerTransformation = Eigen::Affine3f::Identity();
                trackerTransformation = align.getFinalTransformation();

                if (i < boxs.size()) {
                    boxs[i].object.tracker->resetTracking();
                    boxs[i].object.tracker->setTrans(trackerTransformation);
                    boxs[i].object.tracker->compute();
                    boxs[i].object.inScene = true;
                } else {
                    objects[i - boxs.size()].tracker->resetTracking();
                    objects[i - boxs.size()].tracker->setTrans(trackerTransformation);
                    objects[i - boxs.size()].tracker->compute();
                    objects[i - boxs.size()].inScene = true;
                }
            } else {
                if (i < boxs.size()) {
                    boxs[i].object.inScene = true;
                } else {
                    objects[i - boxs.size()].inScene = true;
                }
            }
        } else {
            if (i < boxs.size()) {
                boxs[i].object.inScene = !hideWhenLost;
            } else {
                objects[i - boxs.size()].inScene = !hideWhenLost;
            }
        }

        i++; 
    }
}


// Statistical Outlier Removal (to remove noise)
void statisticalOutlierRemoval(const pcl::PointCloud<point>::ConstPtr &cloud, pcl::PointCloud<point>::Ptr &filteredCloud) {
    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGBA> statisticalOutlierRemoval;
    statisticalOutlierRemoval.setInputCloud(cloud);
    statisticalOutlierRemoval.setMeanK(50);
    statisticalOutlierRemoval.setStddevMulThresh(0.4);
    statisticalOutlierRemoval.filter(*filteredCloud);
}


// Planar segmentation (to remove the floor)
void planarSegmentation(const pcl::PointCloud<point>::ConstPtr &cloud, pcl::PointCloud<point>::Ptr &filteredCloud, double distanceThreshold) {
    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices());

    // Calculates the segmentation
    pcl::SACSegmentation<point> segmentation;
    segmentation.setInputCloud(cloud);
    segmentation.setOptimizeCoefficients(true);
    segmentation.setModelType(pcl::SACMODEL_PLANE);
    segmentation.setMethodType(pcl::SAC_RANSAC);
    segmentation.setMaxIterations(1000);
    segmentation.setDistanceThreshold(distanceThreshold); // Distance can obscure small objects, 0.008 completely removes floor
    segmentation.segment(*inliers, *coefficients);

    if (!(inliers->indices.size() == 0)) {
        // Filters the planar points out
        pcl::ExtractIndices<point> extract;
        extract.setInputCloud(cloud);
        extract.setIndices(inliers);
        extract.setNegative(true);
        extract.filter(*filteredCloud);
    }
}


// Voxelgrid down sampling (for performance)
void voxelGridDownSampling(const pcl::PointCloud<point>::ConstPtr &cloud, pcl::PointCloud<point>::Ptr &filteredCloud, float leafSize) {
    pcl::VoxelGrid<point> voxelGrid;
    voxelGrid.setInputCloud(cloud);
    voxelGrid.setLeafSize(leafSize, leafSize, leafSize);
    voxelGrid.filter(*filteredCloud);
}


// Passthrough filter (cloud bounds)
void passThroughFilter(const pcl::PointCloud<point>::ConstPtr &cloud, pcl::PointCloud<point>::Ptr &filteredCloud, float x, float y, float z) {
    pcl::PassThrough<point> passThroughFilter;
    passThroughFilter.setInputCloud(cloud);

    passThroughFilter.setFilterFieldName("x");
    passThroughFilter.setFilterLimits(-x, x); // X distance from camera.
    passThroughFilter.filter(*filteredCloud);

    passThroughFilter.setInputCloud(filteredCloud);
    passThroughFilter.setFilterFieldName("y");
    passThroughFilter.setFilterLimits(-y, y); // Y distance from camera.
    passThroughFilter.filter(*filteredCloud);

    passThroughFilter.setFilterFieldName("z");
    passThroughFilter.setFilterLimits(0.0, z); // Z distance from camera.
    passThroughFilter.filter(*filteredCloud);
}


// Cloud callback function (called everytime a new input cloud is available)
void cloudCallback(const pcl::PointCloud<point>::ConstPtr &cloud) {
    // Claims the lock so variables aren't interfered with
    std::lock_guard<std::mutex> lock(mtx);

    // Prepares the input cloud for the viewer
    passThroughFilter(cloud, globalCloud, 0.65, 1, 1.80);
    *globalFilteredCloud = *globalCloud;

    // Track the objects and the boxs
    if (count == 0) {
        startTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        frameCount = 0;
        count++;
    } else if (count == 1) {
        // Finds the initial object positions and corrects at a frame interval
        calculateObjectPosition(cloud);
        calculateFPS();
        count++;
        newGlobalCloud = true;
    } else {
        // Prepares the input point cloud for computation
        voxelGridDownSampling(globalFilteredCloud, globalFilteredCloud, globalLeafSize);
        planarSegmentation(globalFilteredCloud, globalFilteredCloud, 0.008);
        statisticalOutlierRemoval(globalFilteredCloud, globalFilteredCloud);

        // Computes the position of the boxs
        for(auto &box: boxs) {
            if (box.object.inScene) {
                box.object.tracker->setInputCloud(globalFilteredCloud);
                box.object.tracker->compute();
                box.object.recordState();
            }
        }

        // Computes the position of the objects
        for(auto &object: objects) {
            if (object.inScene) {
                object.tracker->setInputCloud(globalFilteredCloud);
                object.tracker->compute();
                object.recordState();
            }
        }

        calculateFPS();

        // Triggers the frame interval
        count++;
        if (count > 10 && useCorrection) {
            count = 1;
        }

        newGlobalCloud = true;
    }
} 
//
// Created by Mark Keast (SC18MK)
//

#include "visualisation_callback.h"
#include <mutex>
#include <thread>
#include <pcl/common/transforms.h>

using namespace std::chrono_literals;
using namespace pcl::tracking;


// Visualisation callback function (Called everytime the viewer requires a new frame)
void visualisationCallback(pcl::visualization::PCLVisualizer &visualiser) {
    // Claims the lock so variables aren't interfered with
    std::lock_guard<std::mutex> lock(mtx);
    
    if (!globalCloud) {
        std::this_thread::sleep_for(1s);
        return;
    }

    // Only generates a new frame if the viewer is still open and there is new information
    if (!viewer.wasStopped() && newGlobalCloud) {
        // Add the scene point cloud to the viewer
        if (!visualiser.updatePointCloud (globalCloud, "globalCloud")) {
            visualiser.addPointCloud(globalCloud, "globalCloud");
            visualiser.resetCameraViewpoint("globalCloud");
        }

        // Colours to use for bounding boxs
        int colours[6][3] = {{0, 191, 255}, // Light Blue
                             {255, 0, 0}, // Red
                             {255, 215, 0}, // Gold
                             {0, 255, 0}, // Green
                             {176, 48, 96}, // Maroon
                             {00, 00, 128}}; // Navy 
        
        std::vector<Object> localObjects;
        for (auto &box: boxs) {
            box.inBox.clear();
            box.updateCornerCoordinates();
            localObjects.push_back(box.object);
        }
        localObjects.insert(localObjects.end(), objects.begin(), objects.end());

        int i = 0;
        for(auto &object: localObjects) {
            // Gets the result cloud from tracker
            ParticleXYZRPY result = object.tracker->getResult();
            Eigen::Affine3f transformation = object.tracker->toEigenMatrix(result);
            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr resultCloud(new pcl::PointCloud<pcl::PointXYZRGBA>());
            pcl::transformPointCloud<point>(*(object.tracker->getReferenceCloud()), *resultCloud, transformation);

            // Displays the coloured model reference point cloud
            if (object.inScene) {
                pcl::PointCloud<pcl::PointXYZRGBA>::Ptr boundingCloud(new pcl::PointCloud<pcl::PointXYZRGBA>());
                transformation.translation() += Eigen::Vector3f(0.0f, 0.0f, -0.01f);
                pcl::transformPointCloud<point>(*(object.tracker->getReferenceCloud()), *boundingCloud, transformation);

                pcl::visualization::PointCloudColorHandlerCustom<point> boundingCloudColour(boundingCloud, colours[i][0], colours[i][1], colours[i][2]);
                if (!visualiser.updatePointCloud(boundingCloud, boundingCloudColour, "boundingCloud" + std::to_string(i))) {
                    visualiser.addPointCloud(boundingCloud, boundingCloudColour, "boundingCloud" + std::to_string(i));
                }
            } else {
                visualiser.removePointCloud("boundingCloud" + std::to_string(i));
            }
            
            // Displays the name of the object over the objects
            if (object.inScene) {
                visualiser.removeShape("object3DText" + std::to_string(i), 0);
                result.x -= 0.075;
                result.z -= 0.08;
                visualiser.addText3D("- " + object.name + " -" , result, 0.020, 0, 0, 0, "object3DText" + std::to_string(i), 0);
            } else {
                visualiser.removeShape("object3DText" + std::to_string(i), 0);
            }
            
            // Displays the object statistics
            visualiser.removeShape("objectText" + std::to_string(i), 0);
            visualiser.addText("Object Name: " + object.name + "\n" +
                                    "Current Speed: " + std::to_string(object.currentSpeed) + " m/s\n" + 
                                    "Average Speed: " + std::to_string(object.averageSpeed) + " m/s\n" +
                                    "Roll: " + std::to_string(result.roll) + "\n" +
                                    "Pitch: " + std::to_string(result.pitch) + "\n" +
                                    "Yaw: " + std::to_string(result.yaw), 30,
                                    (120*i) + 30, 15, 1, 1, 1, "objectText" + std::to_string(i), 0);

            // Works out what objects are in each box
            if (i >= boxs.size()) {
                for (auto &box: boxs) {
                    if (box.checkInBox(resultCloud)) {
                        box.inBox.push_back(object.name);
                    }
                }
            } 

            i++;
        }

        // Displays the average FPS
        visualiser.removeShape("averageFPS", 0);
        visualiser.addText("Average FPS: " + std::to_string(averageFPS), 30, (120*localObjects.size()) + 30, 15, 1, 1, 1, "averageFPS", 0);

        // Displays what is in each box
        i = 0;
        for(auto &box: boxs) {
            std::string inBoxString = "In " + box.object.name + ":";
            for (auto &object: box.inBox) {
                inBoxString += " " + object + ",";
            }

            visualiser.removeShape(box.object.name, 0);
            visualiser.addText(inBoxString, 300, (120*i) + 75, 15, 1, 1, 1, box.object.name, 0);

            i++;
        }

        newGlobalCloud = false;
    }
}
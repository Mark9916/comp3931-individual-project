//
// Created by Mark Keast (SC18MK)
//

#ifndef BOX_H
#define BOX_H


#include "object.h"
#include <string>


class Box {
    public:
        Object object;
        std::vector<std::string> inBox;
        float width, height, depth, verticalOffset;
        Box(int amountOfObjects);
        void updateCornerCoordinates();
        bool checkInBox(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr object);
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cornerCloud;
};


#endif //BOX_H